var CryptoJS = require("crypto-js");
var PRIV_KEY = "dd51be93878e891fc37b7d0321728afaa9b37e49";
var PUBLIC_KEY = "d643355e6e356ab008b30227e053b3de";
var async = require('async');
var request= require('request');

exports.gethome = function (req, res) {
  async.waterfall([
    getMarvelResponse,
    prepareData
  ], finish)

  function getMarvelResponse(done) {
        var orden ='-name';
        var name= (req.query.name)?req.query.name:null
        var orden= (req.query.orden)?req.query.orden:null
        var offset= (req.query.offset)?req.query.offset:null
        var ts = new Date().getTime();
        var hash = CryptoJS.MD5(ts + PRIV_KEY + PUBLIC_KEY).toString();
        var url = 'http://gateway.marvel.com:80/v1/public/characters';
        var propertiesObject = {
          apikey: PUBLIC_KEY,
          ts: ts,
          limit:10,
          hash: hash
        };
        if (name) {
          propertiesObject["nameStartsWith"]=  name
        }

        if (orden) {
          propertiesObject["orderBy"]= orden
        }

        if (offset) {
          propertiesObject["offset"]= offset
        }

        request({url:url, qs:propertiesObject, method: "GET"}, function(err, response, body) {
            if(err)  console.log(err);
            return done(null, response, body)
       });
    }
  function prepareData(response, body, done) {
    var data = {
      'response': response,
      'body': body
    }
    var j=JSON.parse(data.body)


    return done(null, j)
  }



  function finish(err, data) {
    res.render('index', {
      'data': data
    })
  }
},

//Metodo para tarer datos de un personajes
exports.getCharacterId = function (req, res) {
  async.waterfall([
    getMarvelId,
    prepareData
  ], finish)

  function getMarvelId(done) {
        // you need a new ts every request
        var ts = new Date().getTime();
        var hash = CryptoJS.MD5(ts + PRIV_KEY + PUBLIC_KEY).toString();
        // the api deals a lot in ids rather than just the strings you want to use
        var characterId = req.query.id;
        var url = 'http://gateway.marvel.com:80/v1/public/characters/'+characterId;
        var propertiesObject = { apikey: PUBLIC_KEY,
        ts: ts,
        hash: hash,
        id: characterId
      };
        request({url:url, qs:propertiesObject, method: "GET"},
        function(err, response, body) {
          if(err)  console.log(err);
          return done(null, response, body)
       });

    }
  function prepareData(response, body, done) {
    var data = {
      'response': response,
      'body': body
    }
    var j=JSON.parse(data.body)
    return done(null, j)
  }
  function finish(err, data) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(data));
    }
},

//Metodo para tarer datos de un personajes
exports.getComic = function (req, res) {
  async.waterfall([
    getComicName,
    prepareData
  ], finish)
  function getComicName(done) {

        // you need a new ts every request
        var ts = new Date().getTime();
        var hash = CryptoJS.MD5(ts + PRIV_KEY + PUBLIC_KEY).toString();
        // the api deals a lot in ids rather than just the strings you want to use
        var url = req.query.uri;
        var propertiesObject = { apikey: PUBLIC_KEY,
        ts: ts,
        hash: hash
      };
        request({url:url, qs:propertiesObject, method: "GET"},
        function(err, response, body) {
          if(err)  console.log(err);
          return done(null, response, body)
       });

    }
  function prepareData(response, body, done) {
    var data = {
      'response': response,
      'body': body
    }
    var j=JSON.parse(data.body)
    return done(null, j)
  }
  function finish(err, data) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(data));
    }
}
