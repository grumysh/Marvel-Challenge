// ExpressJS (Servidor Web)
var express = require('express')
// Request (Para realizar fácilmente peticiones REST)
var request = require('request')
// La aplicación
var app = express();
var homeController = require('./controllers/home');
var bodyParser = require('body-parser');
var methodOverride = require("method-override");
var api = express.Router();



// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Definir EJS como el motor de plantillas predeterminado
app.set('view engine', 'ejs')
// Usar el directorio estático /public para servir archivos
app.use('/assets',express.static(__dirname + '/assets'));/*traer los estilos, scripts y media*/
// Escuchar en la ruta '/' y responder con un llamado a una API
api.get('/', homeController.gethome);
app.use('/', api);

//Ruta para modal de Personaje
api.get('/character',homeController.getCharacterId)

//Ruta para modal de comic
api.get('/comic',homeController.getComic)

app.listen(80);

console.log("Running at Port 80");
